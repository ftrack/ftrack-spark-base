// :copyright: Copyright (c) 2016 ftrack
const webpack = require('webpack');
const path = require('path');
const paths = require('./paths');
const env = require('./env');
const baseConfig = require('./webpack.config.base');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const theme = env['process.env.FTRACK_THEME'] === '"dark"' ? 'dark' : 'light';

if (env['process.env.NODE_ENV'] !== '"production"') {
    throw new Error('Production builds must have NODE_ENV=production.');
}

const htmlPluginConfig = {
    inject: false,
    minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
    },
};

module.exports = Object.assign(baseConfig, {
    mode: 'production',
    entry: [require.resolve('./polyfills'), path.join(paths.source, 'index.js')],
    output: {
        path: paths.build,
        filename: 'static/js/[name].[chunkhash:8].js',
        chunkFilename: 'static/js/[name].[chunkhash:8].chunk.js',
        publicPath: '/',
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: `static/css/theme-${theme}.css`,
        }),
        new HtmlWebpackPlugin(
            Object.assign({}, htmlPluginConfig, {
                template: paths.html,
            })
        ),
        new CopyWebpackPlugin([{ from: paths.static, to: 'static/' }]),
        new webpack.DefinePlugin(env),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
        new webpack.ProvidePlugin({
            fetch: 'exports-loader?self.fetch!whatwg-fetch',
        }),
        new webpack.optimize.AggressiveMergingPlugin(),
    ],
});
