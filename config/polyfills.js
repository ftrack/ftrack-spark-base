// :copyright: Copyright (c) 2016 ftrack

if (typeof Promise === 'undefined') {
    window.Promise = require('promise/lib/es6-extensions.js');
}

// Object.assign() is commonly used with React.
// It will use the native implementation if it's present and isn't buggy.
Object.assign = require('object-assign');

// Polyfills for ES6 features missing in older browsers: IE 11, etc..
require('core-js/fn/array');
require('core-js/fn/map');
require('core-js/fn/string');
require('core-js/fn/number');
require('core-js/fn/symbol');
require('core-js/fn/object/keys.js');

// Polyfills for generator functions (needed for sagas)
// require('regenerator-runtime/runtime');

// CustomEvent polyfill for IE 9/10/11.
require('custom-event-polyfill');

// fetch() polyfill for making API calls.
require('whatwg-fetch');
