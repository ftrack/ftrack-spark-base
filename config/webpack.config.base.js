// :copyright: Copyright (c) 2016 ftrack
const paths = require('./paths');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const isDevelopment = process.env.NODE_ENV !== 'production';
const env = require('./env');
const theme = env['process.env.FTRACK_THEME'] === '"dark"' ? 'dark' : 'light';

module.exports = {
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                enforce: 'pre',
                use: [
                    {
                        loader: 'eslint-loader',
                        options: {
                            ignore: false,
                        },
                    },
                ],
                include: paths.source,
            },
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /(\.scss|\.css)$/,
                use: [
                    {
                        loader: isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            sourceMap: true,
                            importLoaders: 1,
                            localIdentName: '[name]--[local]--[hash:base64:8]',
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            config: { path: paths.postcssConfig },
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            data: `@import "${paths.theme[theme]}";`,
                        },
                    },
                ],
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'image/[path][name].[hash].[ext]',
                            context: paths.source,
                            publicPath: '../../', // relative to index
                        },
                    },
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            bypassOnDebug: true,
                        },
                    },
                ],
            },
        ],
    },
    resolve: {
        extensions: ['*', '.js', '.scss', '.css', '.json'],
        alias: {
            'react-toolbox': '@ftrack/react-toolbox',
        },
        modules: [paths.nodeModules],
    },
};
