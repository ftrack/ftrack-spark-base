// :copyright: Copyright (c) 2016 ftrack

var FTRACK = /^FTRACK_/i;
var NODE_ENV = JSON.stringify(process.env.NODE_ENV || 'development');
var packageDescription = require('../package.json');

module.exports = Object
    .keys(process.env)
    .filter(key => FTRACK.test(key))
    .reduce((env, key) => {
        env['process.env.' + key] = JSON.stringify(process.env[key]);
        return env;
    }, {
        'process.env.NODE_ENV': NODE_ENV,
        'PACKAGE_NAME': JSON.stringify(packageDescription.name),
        'PACKAGE_VERSION': JSON.stringify(packageDescription.version),
    });
