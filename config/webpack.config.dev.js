// :copyright: Copyright (c) 2016 ftrack

const webpack = require('webpack');
const path = require('path');
const env = require('./env');
const paths = require('./paths');
const baseConfig = require('./webpack.config.base');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const theme = env['process.env.FTRACK_THEME'] === '"dark"' ? 'dark' : 'light';

module.exports = Object.assign(baseConfig, {
    mode: 'development',
    entry: [
        `${require.resolve('webpack-dev-server/client')}?/`,
        require.resolve('webpack/hot/dev-server'),
        require.resolve('./polyfills'),
        path.join(paths.source, 'index.js'),
    ],
    output: {
        path: paths.build,
        filename: 'app/[name]/js/[name].bundle.[hash:8].js',
        chunkFilename: 'common/[name].[chunkhash:8].js',
        pathinfo: true,
        publicPath: '/',
    },
    cache: true,
    devtool: 'eval-source-map',
    plugins: [
        new MiniCssExtractPlugin({
            filename: `static/css/theme-${theme}.css`,
        }),
        new HtmlWebpackPlugin({
            inject: false,
            template: paths.html,
        }),
        new CopyWebpackPlugin([
            { from: paths.static, to: 'static/', ignore: [] },
        ]),
        new webpack.DefinePlugin(env),
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
        new webpack.ProvidePlugin({
            fetch: 'exports-loader?self.fetch!whatwg-fetch',
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
});
