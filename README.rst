
#################
ftrack spark base
#################


This is base project which can be used as a starting point for building ftrack
widgets using a React-based UI with components that are ready to be used.

Related projects and documentation:

* `Webpack <https://webpack.github.io/>`_ module loader with development server
  with loader for `CSS Modules <https://github.com/css-modules/css-modules>`_.
* `Babel <babeljs.io>`_ JavaScript compilter with
  `es2015 <https://babeljs.io/docs/learn-es2015/>`_ and
  `react <https://babeljs.io/docs/plugins/preset-react/>`_ presets.
* `ESLint <eslint.org>`_ linter for JS and JSX with
  `Airbnb JavaScript Style Guide <https://github.com/airbnb/javascript>`_
  configuration.
* `React <https://facebook.github.io/react/>`_, library for building user
  interfaces.
* `React toolbox <react-toolbox.com>`_, component library implementing
  `material design <https://design.google.com/>`_ as react components.
* `Building dashboard widgets <https://help.ftrack.com/developing-with-ftrack/key-concepts/widgets>`_
* `ftrack JavaScript API client <http://ftrack-javascript-api.rtd.ftrack.com/en/stable/>`_
* `ftrack spark components <https://bitbucket.org/ftrack/ftrack-spark-components>`_,
   provides some ftrack-specific UI components based on react-toolbox.

Setting up node environment
===========================

You will need a recent version of node (6+) with yarn installed. It is highly
recommended that you also install a version manager for node, such as
`n (Mac OS) <https://github.com/tj/n>`_ or
`nodist (windows) <https://github.com/marcelklehr/nodist>`_. It enables you
can use different node versions in different projects.

Setting up development environment
==================================

1. Fork this repository and check out the new fork.
2. Install dependencies (will run for a few minutes for the first setup)::

    yarn install

3. Start development server

    yarn start

You can view the application on `http://localhost:8001`.

Commands
--------

Start for development::

    yarn start

Build the dist version and copy static files::

    yarn build

You can append `:dark` to the start command to build the dark instead of the
light theme. `yarn run build` will build both themes, but you can specify either
one or the other for a quicker build::

    # Run the development server for the light theme
    yarn run start

    # Run the development server for the dark theme
    yarn run start:dark

    # Build only the light theme
    yarn run build:light

    # Build only the dark theme
    yarn run build:dark

Configuring your editor
=======================

If your editor supports `EditorConfig <http://editorconfig.org/>`_, the
configuration should be picked up automatically. Plugins for several editors
such as Sublime Text, Visual Studio Code and Atom exists.

Syntax highlighting for JavaScript extensions should be extended to add support
for ES2015 and JSX language extensions through Babel.
For sublime text, install the `Babel` package and change the default syntax used
for .js files, by navigating to `View -> Syntax -> Open all with current
extension as -> Babel -> JavaScript (Babel)`.

Next up, you should make sure your editor supplies you with linting information.
For Sublime Text, install the following packages:

* Sublime-Linter
* SublimeLinter-contrib-eslint


Project structure
=================

The project directory structure looks like the following::

  .
  ├── .editorconfig          # Editor configuration to follow style guide.
  ├── .eslintrc              # Linter configuration, based on AirBnb's config.
  ├── config                 # Webpack configuration files
  ├── build                  # Built application for distribution.
  ├── source                 # Application source code
  │   ├── component          # Presentational, "dumb", react components
  │   ├── container          # Components that provide context (e.g. Redux Provider)
  │   ├── layout             # Components that dictate major page structure
  │   ├── static             # Static assets copied to dist (images, etc..)
  │   ├── style              # Global styles and theme configuration
  │   └── index.js           # Application bootstrap and rendering
  │   └── index.html         # Template for index page.
  └── package.json           # Package configuration and dependencies.

Layouts, views and components
-----------------------------

A Layout is something that describes an entire page structure, such as a fixed
navigation, viewport, sidebar, and footer. Most applications will probably only
have one layout, but keeping these components separate makes their intent clear.
Views are components that live at routes, and are generally rendered within a
Layout. What this ends up meaning is that, with this structure, nearly
everything inside of Components ends up being a dumb component.


Theme and styles
================

The base project is configured to use two different themes, a light theme and
a dark theme. The build script will run twice, once for each theme::

    yarn run build

This will generate a theme-light.css and a theme-dark.css file. A JavaScript
snippet included in the index.html template will pick up the query parameter
set by ftrack and load the correct theme based on the user's setting.

To build the project for a single theme, append the theme name to the command,
e.g.::

    yarn run build:light
    yarn run build:dark

Both .scss and .css file extensions are supported out of the box and are
configured to use CSS Modules. After being imported, styles will be processed
with PostCSS for minification and autoprefixing.

Development
===========

Mocked widget API
-----------------

In development mode, the widget API is mocked using the following
environment variables:

    * FTRACK_SERVER
    * FTRACK_API_KEY
    * FTRACK_API_USER
    * FTRACK_ENTITY_ID
    * FTRACK_ENTITY_TYPE
