// :copyright: Copyright (c) 2016 ftrack

import React from 'react';
import 'react-toolbox/lib/commons.scss';
import AppBar from 'react-toolbox/lib/app_bar';
import { Layout, Panel } from 'react-toolbox/lib/layout';

import ExampleContainer from '../../container/example';

import './style.scss';

function RootLayout() {
    return (
        <Layout>
            <Panel>
                <AppBar>My Widget</AppBar>
                <ExampleContainer />
            </Panel>
        </Layout>
    );
}

export default RootLayout;
