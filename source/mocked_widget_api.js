// :copyright: Copyright (c) 2016 ftrack
/* global window */

import log from 'loglevel';

export function mockWidgetLoad(credentials, entity) {
    log.info('Sending mocked widget load event');
    window.postMessage({
        topic: 'ftrack.widget.load',
        data: {
            credentials,
            entity,
        },
    }, '*');
}

export function mockWidgetUpdate(entity) {
    log.info('Sending mocked widget update event');
    window.postMessage({
        topic: 'ftrack.widget.update',
        data: {
            entity,
        },
    }, '*');
}

function isCorsIframe() {
    try {
        const pageHost = window.location.host;
        const topHost = window.top && window.top.location.host;
        if (!topHost || pageHost === topHost) {
            return false;
        }
    } catch (err) {
        // Ignore errors.
    }

    return true;
}

if (process.env.NODE_ENV === 'development') {
    window.addEventListener('DOMContentLoaded', () => {
        if (isCorsIframe()) {
            log.info(
                'In development mode, but running inside an iframe on a ' +
                'different host, not mocking widget API.'
            );
            return;
        }

        log.warn(`In development mode, mocking the widget API using the
following environment variables:

* FTRACK_SERVER
* FTRACK_API_KEY
* FTRACK_API_USER
* FTRACK_ENTITY_ID
* FTRACK_ENTITY_TYPE
`
        );
        const credentials = {
            serverUrl: process.env.FTRACK_SERVER,
            apiKey: process.env.FTRACK_API_KEY,
            apiUser: process.env.FTRACK_API_USER,
        };
        const entity = {
            id: process.env.FTRACK_ENTITY_ID,
            type: process.env.FTRACK_ENTITY_TYPE,
        };

        // Let other `DOMContentLoaded` listeners trigger first.
        setTimeout(() => mockWidgetLoad(credentials, entity), 0);
    });
}
