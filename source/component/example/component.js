import React from 'react';
import PropTypes from 'prop-types';

import EntityLink from 'ftrack-spark-components/lib/entity_link';

function ExampleComponent({ entity }) {
    return (
        <div>
            <EntityLink link={entity && entity.link} size="large" />
        </div>
    );
}

ExampleComponent.propTypes = {
    entity: PropTypes.shape({
        link: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        })),
    }),
};

export default ExampleComponent;
