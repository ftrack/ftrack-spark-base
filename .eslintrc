{
  "root": true,
  "parser": "babel-eslint",
  "extends": "airbnb",
  "parserOptions": {
      "ecmaVersion": 7,
      "sourceType": "module",
      "ecmaFeatures": {
          "jsx": true,
          "experimentalObjectRestSpread": true,
      },
  },
  "ignorePattern": [
      "static/**"
  ],
  "rules": {

      // this option sets a specific tab width for your code
      // https://github.com/eslint/eslint/blob/master/docs/rules/indent.md
      "indent": [2, 4, { "SwitchCase": 1, "VariableDeclarator": 1 }],

      // Do not require JSX file extensions.
      "react/jsx-filename-extension": 0,

      // Validate indentation in JSX
      "react/jsx-indent-props": [2, 4],
      "react/jsx-indent": [2, 4],

      // Require stateless functions when not using lifecycle methods, setState or ref
      // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prefer-stateless-function.md
      "react/prefer-stateless-function": 1,

      "no-param-reassign": [2, {"props": false}],

      // disallow certain syntax forms
      // http://eslint.org/docs/rules/no-restricted-syntax
      //"ForInStatement",
      "no-restricted-syntax": [
        2,
        "DebuggerStatement",
        "LabeledStatement",
        "WithStatement",
      ],

      "no-underscore-dangle": 0,

      "import/no-unresolved": 0,
      "import/no-extraneous-dependencies": 0,

      // Fixed in eslint-config-airbnb/rules/react.js v12.0.0
      // https://github.com/airbnb/javascript/issues/978
      "react/require-extension": "off"
  }
}
